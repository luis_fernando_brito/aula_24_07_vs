trigger OpportunityLineItemTrigger on OpportunityLineItem (before insert) {

 	if(trigger.isBefore && trigger.isInsert){
    	OpportunityLineItemHandler.BeforeInsert(trigger.new);
    }
    
}