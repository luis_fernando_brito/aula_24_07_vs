trigger OpportunityTrigger on Opportunity ( before insert, before delete, after update ) {
    
    if(trigger.isBefore && trigger.isInsert){
    	OpportunityHandler.BeforeInsert(trigger.new);
    }

    if(trigger.isBefore && trigger.isDelete){
    	OpportunityHandler.BeforeDelete(trigger.old);
    }

    if(trigger.isAfter && trigger.isUpdate){
    	OpportunityHandler.AfterUdate( trigger.new, trigger.oldMap );
    }

}