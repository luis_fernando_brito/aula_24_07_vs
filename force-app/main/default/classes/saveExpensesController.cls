public with sharing class saveExpensesController {
   	@AuraEnabled
    public static void saveExpenses(List<Expense__c> ListExpenses){
        Insert ListExpenses;
    }
}