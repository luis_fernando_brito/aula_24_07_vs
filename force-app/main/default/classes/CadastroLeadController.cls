public class CadastroLeadController {

    public leadWrapper leadWR {get; set;}
    
    public list<leadWrapper> listLeadWR {get; set;}
    
    public CadastroLeadController(){
        leadWR = new leadWrapper();
        listLeadWR = new list<leadWrapper>();
        
        for(Lead l : [SELECT ID, FirstName, LastName, Company FROM Lead WHERE OwnerId = :UserInfo.getUserId()]){
            listLeadWR.add(new leadWrapper(l));
        }        
    }
    
    public void criarLead(){
        System.debug('>> leadWR: ' + leadWR);
        Lead l = leadWR.getLead();
        
        Database.SaveResult SR = Database.insert(l);
        
        if(SR.isSuccess()){
            listLeadWR.add(new leadWrapper(l));
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.CONFIRM, 'Lead inserida com sucesso!'));
        }
        else{
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, SR.getErrors()[0].getMessage()));
        }
        
        
        system.debug('>> SR: ' + SR);
    }
}