public class leadDetalhadoController {
    
 
    public String id {get; set;}
    public String firstName {get; set;}
    public String lastName {get; set;}
    public String company {get; set;}

	public leadDetalhadoController(){    
    
		id = apexpages.currentpage().getparameters().get('id');
        firstName = apexpages.currentpage().getparameters().get('firstName');
        lastName = apexpages.currentpage().getparameters().get('lastName');
        company = apexpages.currentpage().getparameters().get('company');
    }

}