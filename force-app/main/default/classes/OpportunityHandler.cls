public class OpportunityHandler {
	
    public static void BeforeInsert(list<Opportunity> listNew){
        list<String> listIdAccount = new list<String>();
    	map<Id, Account> mapAccount = new map<Id, Account>();
        
        for(Opportunity opp : listNew){
	        listIdAccount.add(opp.AccountId);
	    }

	    list<Account> listAcc = [SELECT ID, BillingStreet, BillingPostalcode, BillingCountry FROM Account WHERE ID IN :listIdAccount];

	    for(Account acc : listAcc){
	    	mapAccount.put(acc.Id, acc);
	    }

	    for(Opportunity opp : listNew){
			if(mapAccount.containskey(opp.AccountId)){
				opp.Logradouro__c = mapAccount.get(opp.AccountId).BillingCountry + ' - ' + mapAccount.get(opp.AccountId).BillingStreet;
			   	opp.CEP__c = mapAccount.get(opp.AccountId).BillingPostalcode;
	       	}
	    }
    }

    public static void BeforeDelete( list<Opportunity> oldList ){

    	system.debug('getUserId ' + UserInfo.getUserId() );
    	system.debug('Init BeforeDelete ' + oldList );

        list<String> listIdAccount = new list<String>();
    	map<Id, Account> mapAccount = new map<Id, Account>();
        
        for(Opportunity opp : oldList){
	        listIdAccount.add(opp.AccountId);
	    }

	    list<Account> listAcc = [SELECT ID, OwnerId FROM Account WHERE ID IN :listIdAccount];

	    for(Account acc : listAcc){
	    	mapAccount.put(acc.Id, acc);
	    }

   		for(Opportunity opp : oldList){

			system.debug('Delete Trigger ' + opp );
			system.debug('getUserId ' + UserInfo.getUserId() );
			system.debug('OwnerId ' + opp.OwnerId );

			if( opp.OwnerId != UserInfo.getUserId() ){
				opp.addError('Somente o proprietário pode excluir a oportunidade!');
			}
	    }
    }


    public static void AfterUdate( list<Opportunity> listNew,  Map<Id,Opportunity> oldListMap ){

    	system.debug('Init AfterUdate');
    	system.debug('Init listNew ' + listNew );
    	system.debug('Init oldListMap ' + oldListMap );

    	list<Case> listCase = new list<Case>();

        for(Opportunity opp : listNew){
	        if( oldListMap.get( opp.id ).StageName != opp.StageName && opp.StageName == 'Closed Won' ){
	        	Case caso = new Case();
	        	caso.Status = 'New';
	        	caso.Priority = 'High';
	        	caso.AccountId = opp.AccountId;
	        	listCase.add( caso );
	        }
	    }

	    if( listCase.size() > 0){
	    	insert listCase;
	    	system.debug('Create Cases Success ' + listCase );
	    }	
    }
}