global class leadWebService {
    
        webservice static list<LeadCreateResult> CreateLead(list<LeadWrapper> lead){
        list<Lead> listLeadNew = new list<Lead>();
        list<LeadCreateResult> listACR = new list<LeadCreateResult>();
        
        for(LeadWrapper AW : lead){
            Lead meuLead = new Lead();
            meuLead.FirstName = AW.FirstName;
            meuLead.LastName = AW.LastName;
            meuLead.Company = AW.Company;
            meuLead.Phone = AW.Phone;
            meuLead.Status = 'Open - Not Contacted';
            listLeadNew.add(meuLead);
        }
        
        if(listLeadNew.size() > 0){
            list<Database.saveResult> SRs = Database.insert(listLeadNew, false);
            
            for(Database.SaveResult SR : SRs){
                LeadCreateResult ACR = new LeadCreateResult();
                ACR.RecordId = SR.getId();
                ACR.Sucesso = SR.isSuccess();
                ACR.Mensagem = SR.getErrors().size() > 0 ? SR.getErrors()[0].getMessage() : '';
                listACR.add(ACR);
            }
        }
        
        return listACR;
    }
        
    global class LeadWrapper{
        webservice String FirstName {get; set;}
        webservice String LastName {get; set;}
        webservice String Company {get; set;}
        webservice String Status {get; set;}
        webservice String Phone {get; set;}
    }
    
    global class LeadCreateResult{
        webservice String recordId {get; set;}
        webservice String mensagem {get; set;}
        webservice Boolean Sucesso {get; set;}
    }    
}