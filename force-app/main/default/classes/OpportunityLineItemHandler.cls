public class OpportunityLineItemHandler {

	public static void BeforeInsert( List<OpportunityLineItem> opportunityProductList ){

		system.debug('OpportunityLineItemHandler BeforeInsert INIT');
		system.debug( opportunityProductList );

        Set<Id> oppIds = New Set<Id>(); 
        Map<Id,Id> oppToNewProd = new Map<Id,Id>(); 

        
        for(OpportunityLineItem oppProductItems : opportunityProductList){
        	//if(oppToNewProd.containskey(oppProductItems.OpportunityId)){
	            oppIds.add(oppProductItems.OpportunityId);
	            oppToNewProd.put(oppProductItems.OpportunityId,oppProductItems.Product2Id);
        	//}
        }

        Map<Id,Opportunity> oppWithLineItems = New Map<Id,Opportunity>(
            [Select Id, (Select Id, product2Id From OpportunityLineItems Where Id Not In :opportunityProductList) From Opportunity Where Id In :oppIds] 
        );

        system.debug( 'oppWithLineItems: ' + oppWithLineItems );

        for(OpportunityLineItem oppProductItems : opportunityProductList){ 
            
            if(oppWithLineItems.containskey(oppProductItems.OpportunityId)){

            	Opportunity productTMP = oppWithLineItems.get(oppProductItems.OpportunityId);

	            for(OpportunityLineItem oppProductItemsTMP : productTMP.OpportunityLineItems){
	                //if( oppProductItemsTMP.Product2Id == oppProductItems.Product2Id ){
                	system.debug( 'oppProductItemsTMP.Product2Id: ' + oppProductItemsTMP.Product2Id );
                	//system.debug( 'oppToNewProd.get :' + oppToNewProd.get(oppProductItemsTMP.OpportunityId) );
                	/*if(oppToNewProd.containskey(oppProductItemsTMP.OpportunityId) && oppToNewProd.get(oppProductItemsTMP.OpportunityId).containskey(oppProductItemsTMP.Product2Id)){
	                    oppProductItems.addError('Produto já existe na Oportunidade!');
	                }*/
	            }
            }           

        }

	}

}