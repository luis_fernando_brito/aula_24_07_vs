@isTest 
public class OpportunityHandlerTest {
	@isTest static void testOpportunity() {

		//Pega o perfil padrão
		Profile p = [SELECT Id FROM Profile WHERE Name='Usuário Padrão'];

		//Cria usuário para o Teste
    	User userTest = new User(Alias = '1_test', Email='1_test@tb.com', 
        EmailEncodingKey='UTF-8', LastName='1_test', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='1_test@tb.com');
        insert userTest;

        system.debug( 'Create User in Test: ' + userTest );

		//Cria uma Conta para o Teste e atribui o Onwner da Conta.
		Account newwAccToTest = new Account(Name = 'Account Teste');
		newwAccToTest.BillingStreet = 'Rua Teste';
		newwAccToTest.BillingPostalcode = '000-0000';
		newwAccToTest.BillingCountry = 'Cidade del Teste';
		newwAccToTest.OwnerId = userTest.id;
		insert newwAccToTest;

		system.debug( 'Create Account in Test: ' + newwAccToTest );

		//Cria uma oportunidade para o Teste
		Opportunity newOppToTest = new Opportunity();
		newOppToTest.name = 'Opportunity Test';
		newOppToTest.AccountId = newwAccToTest.Id;
		newOppToTest.StageName = 'Closed Lost';
		newOppToTest.CloseDate = date.today() + 10;
		newOppToTest.Type = 'New Customers';
		insert newOppToTest;

		system.debug( 'Create Opportunity: ' + newOppToTest );

		//Inicia carga de Testes
		Test.startTest();

		newOppToTest.StageName = 'Closed Won';

		system.debug( 'Update Opportunity in Test: ' + newOppToTest );

        update newOppToTest;

        system.debug( 'Update Opportunity in Test: ' + newOppToTest);
        
 		Profile p2 = [SELECT Id FROM Profile WHERE Name='Administrador do sistema'];

        User newUserTest = new User(Alias = 'test', Email='1test@tb.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p2.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='1test@tb.com');

		insert newUserTest;

		system.debug( 'Create UserTest in Test: ' + newOppToTest);

		system.debug( 'Check OwnerId on Account Test: ' + newwAccToTest.ownerId );

		update newOppToTest;

		newwAccToTest.ownerId = newUserTest.id;

		update newwAccToTest;

		system.debug( 'Check Before Update OwnerId on Account Test: ' + newwAccToTest.ownerId );

		system.debug( 'Update OwnerId Opportunity  in Test: ' + newOppToTest);
		
		System.runAs( newUserTest ) {

    		//delete newOppToTest;
    		
    		OpportunityHandler.BeforeDelete( new list<Opportunity>{ newOppToTest} );

    		system.debug( 'Delete Opportunity in Test: ' + newOppToTest );
		}
		//Finaliza os Testes
		Test.stopTest();

    }
}