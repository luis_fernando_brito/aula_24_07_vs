/**
 * @File Name          : leadWrapper.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 29/07/2019 10:46:28
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    29/07/2019 10:46:28   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public class leadWrapper {
    
    public String id {get; set;}
    public String firstName {get; set;}
    public String lastName {get; set;}
    public String company {get; set;}
    
    public leadWrapper(){}
    
    public leadWrapper(lead l){
        this.id = l.Id;
        this.firstName = l.firstName;
        this.lastName = l.lastName;
        this.company = l.company;
    }
    
    public lead getLead(){
        lead l = new lead();
        l.FirstName = this.firstName;
        l.LastName = this.lastName;
        l.Company = this.company;
        return l;
    }
}