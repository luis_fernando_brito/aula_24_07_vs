({
	addExpense : function(component, event, helper) {
        
        var validExpense = component.find('expenseform').reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);
        if(validExpense){
            var newExpense = component.get("v.newExpense");
            console.log("Cria Nova Despesa de Viagem: " + JSON.stringify(newExpense));
            console.log("Send to helper")
            helper.createExpense(component, newExpense);
        }
        
	},
    salveExpenses : function(component, event, helper) {
        
    }
})